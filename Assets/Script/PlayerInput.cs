using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public Card_GameObject CardGameObject;

    void Update()
    {
        if (Card_Manager.instance.m_GameState == "play")
        {

            f_mousePos();
        }
    }
    void f_mousePos()
    {
        Vector3 m_posMouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 m_posCard = this.gameObject.transform.position;


        if (m_posMouse.x - 3 > m_posCard.x)
        {

            CardGameObject.f_inputUI(1);
            Debug.Log("right");

        }
        else if (m_posMouse.x + 3 < m_posCard.x)
        {

            CardGameObject.f_inputUI(2);
            Debug.Log("left");

        }
        else
        {
            CardGameObject.f_inputUI(3);
            Debug.Log("center");

        }

    }

}
