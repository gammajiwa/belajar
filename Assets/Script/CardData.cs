using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Card", menuName = "Card")]
public class CardData : ScriptableObject
{
    public new string m_Title;
    public string m_Des;
    public Sprite m_Art;


    public float m_Food;
    public float m_Happy;
    public float m_Money;
    public float m_Crime;

    public float m_ReverseFood;
    public float m_ReverseHappy;
    public float m_ReverseMoney;
    public float m_ReverseCrime;


}
