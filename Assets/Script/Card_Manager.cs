using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Card_Manager : MonoBehaviour
{
    // Start is called before the first frame update
    public static Card_Manager instance;
    public List<CardData> m_Card = new List<CardData>();
    public List<int> m_Rand = new List<int>();

    // karna proto ini jadi satu dulu disini nanti endingnya bisa banya pake list 
    public CardData m_endCard;


    public int m_IndexCard;


    public string m_GameState;


    public float m_DataMoney;
    public float m_DataHappy;
    public float m_DataCrime;
    public float m_DataFood;


    public float m_ReverseDataMoney;
    public float m_ReverseDataHappy;
    public float m_ReverseDataCrime;
    public float m_ReverseDataFood;


    public Sprite m_DataArt;
    public string m_DataDes;


    public string m_results;
    // public Sprite m_endDisplay;



    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        f_Opening();
    }

    void Update()
    {
        m_DataMoney = m_Card[m_IndexCard].m_Money;
        m_DataHappy = m_Card[m_IndexCard].m_Happy;
        m_DataCrime = m_Card[m_IndexCard].m_Crime;
        m_DataFood = m_Card[m_IndexCard].m_Food;

        m_ReverseDataMoney = m_Card[m_IndexCard].m_ReverseMoney;
        m_ReverseDataHappy = m_Card[m_IndexCard].m_ReverseHappy;
        m_ReverseDataCrime = m_Card[m_IndexCard].m_ReverseCrime;
        m_ReverseDataFood = m_Card[m_IndexCard].m_ReverseFood;

        m_DataArt = m_Card[m_IndexCard].m_Art;
        m_DataDes = m_Card[m_IndexCard].m_Des;

        m_IndexCard = m_Rand[0];

    }

    void f_IndexRandom()
    {
        for (int i = 0; i < m_Card.Count; i++)
        {
            m_Rand.Add(i);
            int index = Random.Range(0, m_Rand.Count);
            int temp = m_Rand[index];
            m_Rand[index] = m_Rand[0];
            m_Rand[0] = temp;
        }

    }
    public void f_Opening()
    {
        m_GameState = "op";
        if (m_GameState == "op")
        {
            f_IndexRandom();
            m_GameState = "play";
            Card_GameObject.FindObjectOfType<Card_GameObject>().f_reseet();
        }
    }
}
