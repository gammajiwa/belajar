using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Card_GameObject : MonoBehaviour
{
    // Start is called before the first frame update
    public Image m_UiMoney;
    public Image m_UiFood;
    public Image m_UiCrime;
    public Image m_UiHappy;



    public float m_tempMoney;
    public float m_tempFood;
    public float m_tempCrime;
    public float m_tempHappy;
    public float m_currMoney;
    public float m_currFood;
    public float m_currCrime;
    public float m_currHappy;


    public GameObject m_UiAccept;
    public GameObject m_UiDecline;
    public GameObject m_BtnReturn;


    public Image m_displayArt;
    public Text m_displayDes;




    void Update()
    {
        f_displayCard();
        f_result();
    }

    void f_currUI()
    {

        m_UiMoney.fillAmount = m_currMoney / 30;
        m_UiFood.fillAmount = m_currFood / 30;
        m_UiCrime.fillAmount = m_currCrime / 30;
        m_UiHappy.fillAmount = m_currHappy / 30;
    }
    void f_tempUI()
    {

        m_UiMoney.fillAmount = m_tempMoney / 30;
        m_UiFood.fillAmount = m_tempFood / 30;
        m_UiCrime.fillAmount = m_tempCrime / 30;
        m_UiHappy.fillAmount = m_tempHappy / 30;
    }

    void f_amount(float money, float food, float happy, float crime)
    {

        m_tempMoney = m_currMoney + money;
        m_tempCrime = m_currCrime + crime;
        m_tempHappy = m_currHappy + happy;
        m_tempFood = m_currFood + food;
    }


    public void f_inputUI(int input)
    {
        if (input == 1)
        {
            f_amount(Card_Manager.instance.m_DataMoney, Card_Manager.instance.m_DataFood,
                    Card_Manager.instance.m_DataHappy, Card_Manager.instance.m_DataCrime);

            f_tempUI();

            f_playerChoice();

            m_UiAccept.gameObject.SetActive(false);
            m_UiDecline.gameObject.SetActive(true);
        }
        if (input == 2)
        {
            f_amount(Card_Manager.instance.m_ReverseDataMoney, Card_Manager.instance.m_ReverseDataFood,
                    Card_Manager.instance.m_ReverseDataHappy, Card_Manager.instance.m_ReverseDataCrime);

            f_tempUI();

            f_playerChoice();

            m_UiAccept.gameObject.SetActive(true);
            m_UiDecline.gameObject.SetActive(false);
        }
        if (input == 3)
        {
            f_amount(0, 0, 0, 0);

            f_currUI();

            m_UiAccept.gameObject.SetActive(false);
            m_UiDecline.gameObject.SetActive(false);
        }

    }


    void f_displayCard()
    {
        if (Card_Manager.instance.m_GameState == "play")
        {
            m_displayArt.sprite = Card_Manager.instance.m_DataArt;
            m_displayDes.text = Card_Manager.instance.m_DataDes;
        }
        if (Card_Manager.instance.m_GameState == "end")
        {
            m_displayArt.sprite = Card_Manager.instance.m_endCard.m_Art;
            m_displayDes.text = Card_Manager.instance.m_results;
        }

    }


    void f_playerChoice()
    {
        if (Input.GetMouseButtonDown(0))
        {
            m_currMoney = m_tempMoney;
            m_currFood = m_tempFood;
            m_currCrime = m_tempCrime;
            m_currHappy = m_tempHappy;


            if (Card_Manager.instance.m_Rand.Count != 1)
            {
                Card_Manager.instance.m_Rand.RemoveAt(0);
            }
            else
            {
                Card_Manager.instance.m_GameState = "end";
            }

        }
    }


    void f_result()
    {
        string food;
        string money;
        string happy;
        string crime;

        if (m_currFood > 10)
        {
            food = "rakyat mu tidak kelaparan";
        }
        else
        {
            food = "rakyat banyak yg kelaparan";
        }

        if (m_currMoney > 10)
        {
            money = "tingkat ekonomi tinggi ";
        }
        else
        {
            money = " tingkat ekonominya rendah";
        }


        if (m_currHappy > 15)
        {
            happy = "rakyat di bawah ke pemimpinanmua mereka sangat bahagia";
        }
        else
        {
            happy = "rakyatmu sendiri membenci mu";
        }

        if (m_currCrime > 15)
        {
            crime = "kejahatan sangat tinggi di bawh kepemimpinan mu";
        }
        else
        {
            crime = "tapi kejahatan sangat rendah di bawah kepemimpinan mu";
        }

        if (Card_Manager.instance.m_GameState == "end")
        {
            m_UiAccept.gameObject.SetActive(false);
            m_UiDecline.gameObject.SetActive(false);
            m_BtnReturn.gameObject.SetActive(true);
            Card_Manager.instance.m_results = "kmu berhasil membuat " + food + " dan " + money + " namun " + happy + " serta " + crime;
        }
    }



    public void f_reseet()
    {
        if (Card_Manager.instance.m_GameState == "play")
        {
            m_currMoney = 10;
            m_currCrime = 10;
            m_currHappy = 10;
            m_currFood = 10;
            m_BtnReturn.gameObject.SetActive(false);
        }
    }

}
